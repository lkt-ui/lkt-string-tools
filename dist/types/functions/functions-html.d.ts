export declare function stripTags(input: string, allowed: string): string;
export declare function htmlEntities(str: string): string;
