export declare function replaceAll(target: string, search: string, replacement: string): string;
export declare function replaceSingleWhiteSpaces(target: string, replacement: string): string;
export declare function trim(text: string, value?: string): string;
