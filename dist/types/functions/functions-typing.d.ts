export declare function isString(value: any): boolean;
export declare function isEmail(str: string): boolean;
export declare function toString(datum: any): string;
export declare function cloneString(str: string): string;
