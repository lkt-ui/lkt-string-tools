export declare function kebabCaseToCamelCase(str?: string): string;
export declare function ucfirst(t: string): string;
export declare function formatNumber(value: number, decimals: number, decimalPoint: string, thousandsSeparator: string): string;
